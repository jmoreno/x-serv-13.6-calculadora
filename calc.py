def sumar(operando1: int, operando2: int) -> int:
    # -- Sumar dos números
    return operando1 + operando2


def restar(operando1: int, operando2: int) -> int:
    # -- Restar dos números
    return operando1 - operando2


print(sumar(1, 2))
print(sumar(3, 4))
print(restar(6, 5))
print(restar(8, 7))
